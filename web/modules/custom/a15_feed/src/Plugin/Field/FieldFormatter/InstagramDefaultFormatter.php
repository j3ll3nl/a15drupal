<?php

namespace Drupal\a15_feed\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\image\Plugin\Field\FieldType\ImageItem;

/**
 * Plugin implementation of the 'instagram_default_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "instagram_default_formatter",
 *   label = @Translation("Instagram default formatter"),
 *   field_types = {
 *     "instagram_field"
 *   }
 * )
 */
class InstagramDefaultFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      // Implement default settings.
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
      // Implement settings form.
    ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Implement settings summary.

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    if(!$images = \Drupal::service('instagram_api.users')->getSelfMediaRecent([],FALSE)) {
      return $elements;
    }

    foreach ($images as $delta => $image) {
      $elements[$delta] =  $this->viewValue($image);
    }

    return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return array
   *   The textual output generated.
   */
  protected function viewValue(Array $image) {
    // The text value has no text format assigned to it, so the user input
    // should equal the output, including newlines.

    $element = [
      '#theme' => 'image',
      '#title' => $image["caption"]["text"],
      '#uri' => $image['images']["standard_resolution"]["url"],
      '#cache' => [
        'tags' => [$image['id']],
      ],
    ];

    return $element;
  }

}
